.EXPORT_ALL_VARIABLES:
COMPOSE_FILE ?= docker/docker-compose-local.yml
COMPOSE_PROJECT_NAME ?= xo_flask

DOTENV_BASE_FILE ?= .env-local
DOTENV_CUSTOM_FILE ?= .env-custom

POETRY_EXPORT_WITHOUT_INDEXES ?= true
POETRY_EXPORT_OUTPUT = requirements.txt
POETRY_VERSION = 1.1.4
POETRY ?= $(HOME)/.poetry/bin/poetry

PYTHON_INSTALL_PACKAGES_USING ?= poetry
MANAGE_COMMAND_LOCATION ?= xo_flask/manage.py

mkfile_path := $(abspath $(lastword $(MAKEFILE_LIST)))
mkfile_dir := $(dir $(mkfile_path))

export PYTHONPATH=:mkfile_dir

-include $(DOTENV_BASE_FILE)
-include $(DOTENV_CUSTOM_FILE)

.PHONY: install-poetry
install-poetry:
	curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python
	$(POETRY) config virtualenvs.create false
	$(POETRY) run pip install --upgrade pip

.PHONY: install-packages
install-packages:
	$(POETRY) install -vv $(opts)

.PHONY: install
install: install-poetry install-packages

.PHONY: update
update:
	$(POETRY) update -v

.PHONY: service
service:
	$(POETRY) run python xo_flask

.PHONY: docker-lint
docker-lint:
	docker run --rm -i replicated/dockerfilelint:ad65813 < ./docker/Dockerfile

.PHONY: docker-build
docker-build:
	@docker build \
		--tag=xo-flask \
		--file=docker/Dockerfile \
		--build-arg BUILD_RELEASE=dev \
		.

.PHONY: docker-up
docker-up:
	docker-compose up --remove-orphans -d
	docker-compose ps

.PHONY: docker-down
docker-down:
	docker-compose down

.PHONY: docker-logs
docker-logs:
	docker-compose logs --follow

.PHONY: migrate
migrate:
	python $(MANAGE_COMMAND_LOCATION) db upgrade head

.PHONY: migrations
migrations:
	python $(MANAGE_COMMAND_LOCATION) db migrate --message auto

.PHONY: downgrade
downgrade:
	python $(MANAGE_COMMAND_LOCATION) db downgrade -1

# command example: `make downgrade_to MIGRATION='fb1f4b788f03'`
.PHONY: downgrade-to
downgrade-to:
	python $(MANAGE_COMMAND_LOCATION) db downgrade ${MIGRATION}

.PHONY: lint-bandit
lint-bandit:
	$(POETRY) run bandit --ini .bandit --recursive

.PHONY: lint-black
lint-black:
	$(POETRY) run black --check --diff .

.PHONY: lint-flake8
lint-flake8:
	$(POETRY) run flake8

.PHONY: lint-isort
lint-isort:
	$(POETRY) run isort --check-only --diff .

.PHONY: lint-mypy
lint-mypy:
	$(POETRY) run mypy

.PHONY: lint
lint: lint-bandit lint-black lint-flake8 lint-isort lint-mypy

.PHONY: fmt
fmt:
	$(POETRY) run isort .
	$(POETRY) run black .

.PHONY: test
test:
	$(POETRY) run pytest $(opts) $(call tests,.)
