import pytest

from xo_flask.manage import app as application


@pytest.fixture
def app():
    application.config.from_object("xo_flask.config.TestingConfig")
    return application
