from datetime import datetime as dt
from http import HTTPStatus

import pytest


@pytest.mark.usefixtures("client_class")
class TestAuth:
    def test_registered_user_login__ok__(self):
        """
        Test for login of registered-user login
        """
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"
        # user registration
        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        # registered user login
        login_response = self.client.post(
            "/api/v1/auth/login",
            json=dict(
                email=email,
                password="123456",
            ),
        )
        data = login_response.json
        assert bool(data["Authorization"]) is True
        assert login_response.status_code == HTTPStatus.OK

    def test_valid_logout__ok__(self):
        """
        Test for logout before token expires
        """

        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"
        # user registration
        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        # registered user login
        login_response = self.client.post(
            "/api/v1/auth/login",
            json=dict(
                email=email,
                password="123456",
            ),
        )
        data = login_response.json
        assert bool(data["Authorization"]) is True
        assert login_response.status_code == 200

        # valid token logout
        response = self.client.post(
            "/api/v1/auth/logout",
            headers=dict(
                Authorization="Bearer " + login_response.json["Authorization"],
            ),
        )
        data = response.json
        assert data["status"] == "success"
        assert response.status_code == HTTPStatus.OK

    def test_valid_logout__fail__invalid_token(self):
        """
        Test for logout with wrong token
        """

        # invalid token logout
        response = self.client.post(
            "/api/v1/auth/logout",
            headers=dict(
                Authorization="Bearer " + "some wrong value",
            ),
        )
        data = response.json
        assert data["status"] == "fail"
        assert data["message"] == "Invalid token. Please log in again."
        assert response.status_code == HTTPStatus.UNAUTHORIZED
