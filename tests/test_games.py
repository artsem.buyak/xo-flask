from datetime import datetime as dt
from http import HTTPStatus

import pytest


@pytest.mark.usefixtures("client_class")
class TestGames:
    def test_create_game__ok__(self):
        """
        Test create game
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        response = self.client.post(
            "/api/v1/games/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.CREATED
        assert bool(response.json["game_uuid"]) is True

    def test_create_game__fail__unauthorized(self):
        """
        Test create game - fail
        """

        response = self.client.post(
            "/api/v1/games/create",
            headers=dict(
                Authorization="Bearer some wrong value",
            ),
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED
        assert bool(response.json["game_uuid"]) is False

    def test_create_game_list__ok__(self):
        """
        Test create game
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        for _ in range(5):
            self.client.post(
                "/api/v1/games/create",
                headers=dict(
                    Authorization="Bearer " + response_data["Authorization"],
                ),
            )

        response = self.client.get(
            "/api/v1/games/",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.OK
        assert len(response.json["data"]) == 5

    def test_create_game_list__ok__empty_game_list(self):
        """
        Test create game
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        response = self.client.get(
            "/api/v1/games/",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.OK
        assert len(response.json["data"]) == 0
