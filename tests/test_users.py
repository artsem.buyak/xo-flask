from datetime import datetime as dt
from http import HTTPStatus

import pytest

from xo_flask import database
from xo_flask.db.models import User


@pytest.mark.usefixtures("client_class")
class TestUsers:
    def test_encode_auth_token__ok__(self):
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"

        user = User(
            email=email,
            password="test",
            registered_on=dt.utcnow(),
        )
        database.session.add(user)
        database.session.commit()
        auth_token = user.encode_auth_token(user.id)
        assert isinstance(auth_token, str) is True

    def test_decode_auth_token__ok__(self):
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"

        user = User(
            email=email,
            password="test",
            registered_on=dt.utcnow(),
        )
        database.session.add(user)
        database.session.commit()
        auth_token = user.encode_auth_token(user.id)
        assert isinstance(auth_token, str) is True
        assert User.decode_auth_token(auth_token) == user.id

    def test_retrieve_user__ok__(self):
        """
        Test retrieve user
        """

        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        # retrieve user info
        response = self.client.get(
            "/api/v1/users/retrieve",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.OK
        assert response.json["email"] == email
        assert response.json["username"] == username

    def test_retrieve_user_by_public_id__ok__(self):
        """
        Test retrieve user
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        user = User.query.filter_by(email=email).first()
        user.admin = True
        database.session.commit()

        response = self.client.get(
            f"/api/v1/users/retrieve/{user.public_id}",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.OK
        assert response.json["email"] == email
        assert response.json["username"] == username

    def test_retrieve_user_by_public_id__fail__not_admin(self):
        """
        Test retrieve user not from admin token
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        user = User.query.filter_by(email=email).first()

        response = self.client.get(
            f"/api/v1/users/retrieve/{user.public_id}",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED
