from datetime import datetime as dt
from http import HTTPStatus

import pytest

from xo_flask import database
from xo_flask.db.constants import GameStatusType, GameWinnerType
from xo_flask.db.models import Game


@pytest.mark.usefixtures("client_class")
class TestMoves:
    def test_create_move__ok__(self):
        """
        Test create move
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        response = self.client.post(
            "/api/v1/games/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.CREATED
        assert bool(response.json["game_uuid"]) is True

        game_uuid = response.json["game_uuid"]

        response_move = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
            json=dict(
                game_uuid=game_uuid,
                px=1,
                py=1,
            ),
        )
        assert response_move.status_code == HTTPStatus.CREATED
        assert response_move.json["message"] == "Move created"

    def test_create_move__fail__unauthorized(self):
        """
        Test create game - fail
        """

        response = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer some wrong value",
            ),
            json=dict(
                game_uuid="some_value_without_any_sense",
                px=1,
                py=1,
            ),
        )

        assert response.status_code == HTTPStatus.UNAUTHORIZED

    def test_create_move__fail__bad_request_params(self):
        """
        Test create game - bad request params
        """

        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        response = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer some wrong value",
            ),
        )

        assert response.status_code == HTTPStatus.BAD_REQUEST

    def test_create_move__fail__cell_busy(self):
        """
        Test fail - cell busy by yourself
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        response = self.client.post(
            "/api/v1/games/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.CREATED
        assert bool(response.json["game_uuid"]) is True

        game_uuid = response.json["game_uuid"]

        response_move = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
            json=dict(
                game_uuid=game_uuid,
                px=1,
                py=1,
            ),
        )
        assert response_move.status_code == HTTPStatus.CREATED
        assert response_move.json["message"] == "Move created"

        response_the_same_cell = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
            json=dict(
                game_uuid=game_uuid,
                px=1,
                py=1,
            ),
        )
        assert response_the_same_cell.status_code == HTTPStatus.BAD_REQUEST
        assert response_the_same_cell.json["message"] == "Cell is busy, choose another!"

    def test_create_move__fail__cell_busy_by_computer(self):
        """
        Test fail - cell busy by computer
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        response = self.client.post(
            "/api/v1/games/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.CREATED
        assert bool(response.json["game_uuid"]) is True

        game_uuid = response.json["game_uuid"]

        response_move = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
            json=dict(
                game_uuid=game_uuid,
                px=1,
                py=1,
            ),
        )
        assert response_move.status_code == HTTPStatus.CREATED
        assert response_move.json["message"] == "Move created"
        px = int(response_move.json["px"])
        py = int(response_move.json["py"])

        response_the_same_cell = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
            json=dict(
                game_uuid=game_uuid,
                px=px,
                py=py,
            ),
        )
        assert response_the_same_cell.status_code == HTTPStatus.BAD_REQUEST
        assert response_the_same_cell.json["message"] == "Cell is busy, choose another!"

    def test_create_move__fail__game_finished(self):
        """
        Test fail - cell busy by yourself
        """
        # user registration
        email = f"bidon+{dt.now().strftime('%m%d%Y%H%M%S%f')}@pomoev.com"
        username = f"pomoev+{dt.now().strftime('%m%d%Y%H%M%S%f')}"

        user_response = self.client.post(
            "/api/v1/users/signup",
            json=dict(
                email=email,
                username=username,
                password="123456",
            ),
        )
        response_data = user_response.json
        assert bool(response_data["Authorization"]) is True
        assert user_response.status_code == HTTPStatus.CREATED

        response = self.client.post(
            "/api/v1/games/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
        )

        assert response.status_code == HTTPStatus.CREATED
        assert bool(response.json["game_uuid"]) is True

        game_uuid = response.json["game_uuid"]

        game = Game.query.filter_by(game_uuid=game_uuid).first()
        game.status = GameStatusType.FINISHED
        game.winner = GameWinnerType.TIE
        database.session.commit()

        response_move = self.client.post(
            "/api/v1/moves/create",
            headers=dict(
                Authorization="Bearer " + response_data["Authorization"],
            ),
            json=dict(
                game_uuid=game_uuid,
                px=1,
                py=1,
            ),
        )

        assert response_move.status_code == HTTPStatus.BAD_REQUEST
        assert response_move.json["message"] == "The game finished. Winner: tie"
