# XO Flask

## Before installation

This projects uses the following standard tools:

- [`poetry`](https://python-poetry.org/docs/) — to manage Python project (install dependencies, run tests and linters);
- [`docker`](https://www.docker.com/) — to run external services like databases, caches, etc.

## Installation

Install Poetry and Python dependencies:

```shell
$ make install
```

## Configuration and launch

Run migrations:

```shell
$ make migrate
```

Run application:

```shell
$ make service
```

## Updating and development

To update application dependencies, run:

```shell
$ make update
```

It will produce your `poetry.lock`, with all dependencies (and all underlying dependencies) pinned.

To create new migration from your DB model run:

```shell
$ make migrations
```

## Testing and checking code style

For tests it's enough to run:

```shell
$ make test
```

Before submitting a PR, please also run:

```shell
$ make lint
```

Note, that for all repositories in GitLab code style check is enforced, so any PR will be blocked until style issues aren't resolved.

## Docker image

Docker image is used for both - running in production and on developers' machines as dependency (use `docker-compose`). Here's how you can build docker image locally.

If you have made any changes to `Dockerfile`, first list it:

```shell
$ make docker-lint
```

Then run the build:

```shell
$ make docker-build
```

## Api description

See description here:
[openapi file](https://gitlab.com/artsem.buyak/xo-flask/-/blob/master/api_description/openapi.yaml)
