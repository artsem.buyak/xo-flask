import os
import unittest

from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from xo_flask import create_app, database
from xo_flask.db.models import BlacklistToken, User
from xo_flask.routers import blueprint

app = create_app(os.getenv("PROJECT_ENV") or "dev")

app.register_blueprint(blueprint)

app.app_context().push()

manager = Manager(app)

migrate = Migrate(
    app,
    database,
    directory=os.path.join("xo_flask", "db", "migrations"),
)

manager.add_command("db", MigrateCommand)


@manager.command
def run():
    app.run()


@manager.command
def test():
    """Runs the unit tests."""

    tests = unittest.TestLoader().discover("app/test", pattern="test*.py")
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        return 0
    return 1


if __name__ == "__main__":
    print(BlacklistToken, User)

    manager.run()
