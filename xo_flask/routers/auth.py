from flask import request
from flask_restplus import Resource

from xo_flask.db.dto.auth import AuthDto
from xo_flask.db.models import User
from xo_flask.db.services.blacklist_service import save_token

api = AuthDto.api
user_auth = AuthDto.user_auth


class Auth:
    @staticmethod
    def login_user(data):
        try:
            # fetch the user data
            user = User.query.filter_by(email=data.get("email")).first()
            if user and user.check_password(data.get("password")):
                auth_token = user.encode_auth_token(user.id)
                if auth_token:
                    response_object = {
                        "status": "success",
                        "message": "Successfully logged in.",
                        "Authorization": auth_token,
                    }
                    return response_object, 200
            else:
                response_object = {
                    "status": "fail",
                    "message": "email or password does not match.",
                }
                return response_object, 401

        except Exception as e:
            print(e)
            response_object = {
                "status": "fail",
                "message": "Try again",
            }
            return response_object, 500

    @staticmethod
    def logout_user(data):
        if data:
            auth_token = data.split(" ")[1]
        else:
            auth_token = ""  # nosec
        if auth_token:
            resp = User.decode_auth_token(auth_token)
            if not isinstance(resp, str):
                # mark the token as blacklisted
                return save_token(token=auth_token)
            else:
                response_object = {
                    "status": "fail",
                    "message": resp,
                }
                return response_object, 401
        else:
            response_object = {
                "status": "fail",
                "message": "Provide a valid auth token.",
            }
            return response_object, 403

    @staticmethod
    def get_logged_in_user(new_request):
        # get the auth token
        auth_token = new_request.headers.get("Authorization")
        if auth_token:
            resp = User.decode_auth_token(auth_token[auth_token.find(" ") + 1 :])
            if not isinstance(resp, str):
                user = User.query.filter_by(id=resp).first()
                response_object = {
                    "status": "success",
                    "data": {
                        "user_id": user.id,
                        "email": user.email,
                        "admin": user.admin,
                        "public_id": user.public_id,
                        "registered_on": str(user.registered_on),
                    },
                }
                return response_object, 200
            response_object = {
                "status": "fail",
                "message": resp,
            }
            return response_object, 401
        else:
            response_object = {
                "status": "fail",
                "message": "Provide a valid auth token.",
            }
            return response_object, 403


@api.route("/login")
class UserLogin(Resource):
    """
    User Login Resource
    """

    @api.doc("user login")
    @api.expect(user_auth, validate=True)
    def post(self):
        # get the post data

        post_data = request.json
        return Auth.login_user(data=post_data)


@api.route("/logout")
class LogoutAPI(Resource):
    """
    Logout Resource
    """

    @api.doc("logout a user")
    def post(self):
        # get auth token

        auth_header = request.headers.get("Authorization")
        return Auth.logout_user(data=auth_header)
