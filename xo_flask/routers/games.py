from random import SystemRandom

from flask_restplus import Resource

from xo_flask.db.dto.game import GameDto
from xo_flask.db.services.game_service import create_new_game, get_all_user_games
from xo_flask.db.services.move_service import create_new_move
from xo_flask.routers.decorators import token_required

api = GameDto.api
_game = GameDto.game
_game_response = GameDto.game_info
cryptogen = SystemRandom()


@api.route("/")
class GameList(Resource):
    @api.doc("list_of_registered_games")
    @api.marshal_list_with(_game_response, envelope="data")
    @token_required
    def get(self, token_data: dict):
        """
        List all games by user (token required)
        """

        return get_all_user_games(user_id=token_data["user_id"])


@api.route("/create")
class GameCreate(Resource):
    @api.response(201, "Game successfully created.")
    @api.doc("create a new game")
    @api.marshal_with(_game)
    @token_required
    def post(self, token_data: dict):
        """
        Creates a new Game (token required)
        """

        game = create_new_game(data=token_data)
        response_object = {
            "game_uuid": game.game_uuid,
            # coordination of computer move
            "initial_px": None,
            "initial_py": None,
        }

        # Randomly choose who starts
        if cryptogen.randrange(2):
            create_new_move(
                game_uuid=game.game_uuid,
                px=0,
                py=0,
                is_user=False,
            )
            response_object["initial_px"] = 0
            response_object["initial_py"] = 0

        return response_object, 201
