from flask import request
from flask_restplus import Resource

from xo_flask.db.dto.users import UserDto
from xo_flask.db.services.user_service import get_a_user, get_all_users, save_new_user
from xo_flask.routers.decorators import admin_token_required, token_required

api = UserDto.api
_user = UserDto.user


@api.route("/")
class UserList(Resource):
    @api.doc("list_of_registered_users")
    @api.marshal_list_with(_user, envelope="data")
    @admin_token_required
    def get(self):
        """
        List all registered users (ADMIN ONLY)
        """

        return get_all_users()


@api.route("/retrieve")
@api.response(404, "User not found.")
@api.response(401, "Unauthorized: No permission - see authorization schemes")
class User(Resource):
    @api.doc("get a user")
    @api.marshal_with(_user)
    @token_required
    def get(self, token_data: dict):
        """
        Get own user info by token (token required)
        """
        user = get_a_user(token_data["public_id"])
        if not token_data:
            api.abort(404)
        else:
            return user


@api.route("/retrieve/<public_id>")
@api.param("public_id", "The User identifier")
@api.response(404, "User not found.")
@api.response(401, "Unauthorized: No permission - see authorization schemes")
class UserByAdmin(Resource):
    @api.doc("get a user")
    @api.marshal_with(_user)
    @admin_token_required
    def get(self, public_id):
        """
        Get a user given its identifier (ADMIN ONLY)
        """

        user = get_a_user(public_id)
        if not user:
            api.abort(404)
        else:
            return user


@api.route("/signup")
class UserSignup(Resource):
    @api.response(201, "User successfully created.")
    @api.doc("create a new user")
    @api.expect(_user, validate=True)
    def post(self):
        """
        Creates a new User
        """

        data = request.json
        return save_new_user(data=data)
