from flask import request
from flask_restplus import Resource

from xo_flask.db.constants import WINNER_MAPPING, GameStatusType
from xo_flask.db.dto.move import MoveDto
from xo_flask.db.services.game_service import (
    get_game_by_game_uuid,
    update_game_status_winner,
)
from xo_flask.db.services.move_service import (
    create_new_move,
    get_all_moves_by_game_uuid,
)
from xo_flask.main.ai import ArtificialIntelligence
from xo_flask.routers.decorators import token_required

api = MoveDto.api
_move = MoveDto.move
result_message_dict = {
    "X": "The winner is user! Congratulations!",
    "O": "The winner is computer! Don't give up, try again)!",
}


@api.route("/create")
class MoveCreate(Resource):
    @api.response(201, "Move successfully created.")
    @api.doc("create a new move")
    @api.expect(_move, validate=True)
    @token_required
    def post(self, token_data: dict):
        """
        Create a new Move
        """

        data = request.json

        game = get_game_by_game_uuid(game_uuid=data["game_uuid"])
        if game.status == GameStatusType.FINISHED:
            response_object = {
                "status": "fail",
                "message": f"The game finished. Winner: {game.winner}",
            }
            return response_object, 400
        elif game is None:
            response_object = {
                "status": "fail",
                "message": "Game doesn't exist",
            }
            return response_object, 409

        current_game_position = ArtificialIntelligence()
        moves = get_all_moves_by_game_uuid(game=game)

        # event sourcing from moves list
        current_game_position.position_from_moves_list(moves)

        if not current_game_position.is_valid(data["px"], data["py"]):
            response_object = {
                "status": "fail",
                "message": "Cell is busy, choose another!",
            }
            return response_object, 400

        # create user move
        create_new_move(**data, is_user=True)
        current_game_position.current_state[data["px"]][data["py"]] = "X"

        # check that game didn't finish after human move
        is_end = current_game_position.is_end()
        if is_end is not None:
            # update game status
            update_game_status_winner(
                game_uuid=game.game_uuid,
                winner=WINNER_MAPPING[is_end],
            )

            message = result_message_dict.get(is_end, "It's a tie!")
            response_object = {
                "status": "success",
                "message": f"The game finished. {message}",
            }
            return response_object, 201
        else:
            _, px, py = current_game_position.max_alpha_beta(-2, 2)
            create_new_move(
                game_uuid=game.game_uuid,
                px=px,
                py=py,
                is_user=False,
            )
            # check that game didn't finish after computer move
            is_end = current_game_position.is_end()
            if is_end is not None:
                # update game status
                update_game_status_winner(
                    game_uuid=game.game_uuid,
                    winner=WINNER_MAPPING[is_end],
                )

                message = result_message_dict.get(is_end, "It's a tie!")
                response_object = {
                    "status": "success",
                    "message": f"The game finished. {message}",
                }
                return response_object, 201

            response_object = {
                "status": "success",
                "message": "Move created",
                # coordination of computer move
                "px": px,
                "py": py,
            }
            return response_object, 201
