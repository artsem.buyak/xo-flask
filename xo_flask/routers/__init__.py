from flask import Blueprint
from flask_restplus import Api

from xo_flask.routers.auth import api as auth_ns
from xo_flask.routers.games import api as game_ns
from xo_flask.routers.moves import api as move_ns
from xo_flask.routers.users import api as user_ns

blueprint = Blueprint("api", __name__)

api = Api(
    blueprint,
    title="FLASK RESTPLUS API TIC TAC TOE",
    version="1.0",
    description="a boilerplate for flask restplus web service",
)

api.add_namespace(auth_ns, path="/api/v1/auth")
api.add_namespace(game_ns, path="/api/v1/games")
api.add_namespace(user_ns, path="/api/v1/users")
api.add_namespace(move_ns, path="/api/v1/moves")
