from datetime import datetime, timedelta

import jwt

from xo_flask import database, flask_bcrypt
from xo_flask.config import key
from xo_flask.db.constants import GameStatusType, GameWinnerType


class User(database.Model):
    """
    User Model for storing user related details
    """

    __tablename__ = "user"

    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    email = database.Column(database.String(255), unique=True, nullable=False)
    registered_on = database.Column(database.DateTime, nullable=False)
    admin = database.Column(database.Boolean, nullable=False, default=False)
    public_id = database.Column(database.String(100), unique=True)
    username = database.Column(database.String(50), unique=True)
    password_hash = database.Column(database.String(100))

    @property
    def password(self):
        raise AttributeError("password: write-only field")

    @password.setter
    def password(self, password):
        self.password_hash = flask_bcrypt.generate_password_hash(password).decode(
            "utf-8"
        )

    def check_password(self, password):
        return flask_bcrypt.check_password_hash(self.password_hash, password)

    def encode_auth_token(self, user_id):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                "exp": datetime.utcnow() + timedelta(days=1, seconds=5),
                "iat": datetime.utcnow(),
                "sub": user_id,
            }
            return jwt.encode(payload, key, algorithm="HS256")
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token):
        """
        Decodes the auth token
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, key, algorithms="HS256")
            is_blacklisted_token = BlacklistToken.check_blacklist(auth_token)
            if is_blacklisted_token:
                return "Token blacklisted. Please log in again."
            else:
                return payload["sub"]
        except jwt.ExpiredSignatureError:
            return "Signature expired. Please log in again."
        except jwt.InvalidTokenError:
            return "Invalid token. Please log in again."

    def __repr__(self):
        return f"<User {self.username}>"


class BlacklistToken(database.Model):
    """
    Token Model for storing JWT tokens
    """

    __tablename__ = "blacklist_tokens"

    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    token = database.Column(database.String(500), unique=True, nullable=False)
    blacklisted_on = database.Column(database.DateTime, nullable=False)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.now()

    def __repr__(self):
        return "<id: token: {}".format(self.token)

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted

        res = BlacklistToken.query.filter_by(token=str(auth_token)).first()
        if res:
            return True
        else:
            return False


class Game(database.Model):
    """
    History of all games and the players tied to those games.
    """

    __tablename__ = "game"

    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    game_uuid = database.Column(database.String(100), unique=True)
    status = database.Column(
        database.Enum(GameStatusType),
        nullable=False,
        default=GameStatusType.IN_PROGRESS.value,
        index=True,
    )
    winner = database.Column(database.Enum(GameWinnerType), nullable=True)
    user_id = database.Column(
        database.Integer,
        database.ForeignKey(User.id),
        index=True,
    )

    user = database.relationship("User", foreign_keys="Game.user_id")
    created_at = database.Column(
        database.DateTime,
        default=datetime.utcnow,
    )
    finished_at = database.Column(
        database.DateTime,
        nullable=True,
    )


class Move(database.Model):
    """
    History of all moves.
    """

    id = database.Column(database.Integer, primary_key=True, autoincrement=True)
    game_id = database.Column(
        database.Integer,
        database.ForeignKey(Game.id),
        index=True,
    )
    px = database.Column(database.Integer, nullable=False, default=0)
    py = database.Column(database.Integer, nullable=False, default=0)
    is_user = database.Column(database.Boolean, nullable=False, default=True)
    created_at = database.Column(
        database.DateTime,
        default=datetime.utcnow,
    )

    user = database.relationship("Game", foreign_keys="Move.game_id")
