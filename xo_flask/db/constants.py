import enum


@enum.unique
class GameStatusType(str, enum.Enum):
    IN_PROGRESS = "in_progress"
    FINISHED = "finished"


@enum.unique
class GameWinnerType(str, enum.Enum):
    COMPUTER = "computer"
    USER = "user"
    TIE = "tie"


WINNER_MAPPING = {
    "X": GameWinnerType.USER,
    "O": GameWinnerType.COMPUTER,
    ".": GameWinnerType.TIE,
}
