import datetime
import uuid

from xo_flask import database
from xo_flask.db.models import User
from xo_flask.db.services.blacklist_service import generate_token


def save_new_user(data):
    user = User.query.filter_by(email=data["email"]).first()
    if not user:
        new_user = User(
            public_id=str(uuid.uuid4()),
            email=data["email"],
            username=data["username"],
            password=data["password"],
            registered_on=datetime.datetime.utcnow(),
        )
        save_changes(new_user)
        return generate_token(user=new_user)
    else:
        response_object = {
            "status": "fail",
            "message": "User already exists. Please Log in.",
        }
        return response_object, 409


def get_all_users():
    return User.query.all()


def get_a_user(public_id):
    return User.query.filter_by(public_id=public_id).first()


def drop_all_users():
    User.query.delete()


def save_changes(data):
    database.session.add(data)
    database.session.commit()
