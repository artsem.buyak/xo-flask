import uuid

from xo_flask import database
from xo_flask.db.constants import GameStatusType, GameWinnerType
from xo_flask.db.models import Game


def create_new_game(data):

    new_game = Game(
        game_uuid=str(uuid.uuid4()),
        user_id=data["user_id"],
        status=GameStatusType.IN_PROGRESS,
    )
    print(new_game.status)
    save_changes(new_game)

    return new_game


def get_game_by_game_uuid(game_uuid: str):
    return Game.query.filter_by(game_uuid=game_uuid).first()


def get_all_user_games(user_id):
    return Game.query.filter_by(user_id=user_id).all()


def update_game_status_winner(game_uuid: str, winner: GameWinnerType):
    # update winner and set finished game status
    game = Game.query.filter_by(game_uuid=game_uuid).first()
    game.status = GameStatusType.FINISHED
    game.winner = winner.value
    database.session.commit()


def save_changes(data):
    database.session.add(data)
    database.session.commit()
