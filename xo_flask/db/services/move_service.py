from xo_flask import database
from xo_flask.db.models import Game, Move


def create_new_move(**data):
    """
    Create new move using px and py positions
    """

    game = Game.query.filter_by(game_uuid=data["game_uuid"]).first()
    if game:
        new_move = Move(
            game_id=game.id,
            is_user=data["is_user"],
            px=data["px"],
            py=data["py"],
        )
        save_changes(new_move)

        return new_move

    else:
        response_object = {
            "status": "fail",
            "message": "Game doesn't. exist",
        }
        return response_object, 409


def get_all_moves_by_game_uuid(game):
    """
    Get list of moves by `group_uuid`
    """

    return Move.query.filter_by(game_id=game.id).all()


def save_changes(data):
    database.session.add(data)
    database.session.commit()
