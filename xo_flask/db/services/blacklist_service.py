from xo_flask import database
from xo_flask.db.models import BlacklistToken


def save_token(token):
    blacklist_token = BlacklistToken(token=token)
    try:
        # insert the token
        database.session.add(blacklist_token)
        database.session.commit()
        response_object = {
            "status": "success",
            "message": "Successfully logged out.",
        }
        return response_object, 200
    except Exception as e:
        response_object = {
            "status": "fail",
            "message": e,
        }
        return response_object, 500


def generate_token(user):
    try:
        # generate the auth token
        auth_token = user.encode_auth_token(user.id)
        response_object = {
            "status": "success",
            "message": "Successfully registered.",
            "Authorization": auth_token,
        }
        return response_object, 201
    except Exception:
        response_object = {
            "status": "fail",
            "message": "Some error occurred. Please try again.",
        }
        return response_object, 500
