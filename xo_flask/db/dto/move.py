from flask_restplus import Namespace, fields


class MoveDto:
    api = Namespace("move", description="move related operations")
    move = api.model(
        "move",
        {
            "game_uuid": fields.String(required=True, description="game uuid"),
            "px": fields.Integer(description="position on the axis Ox"),
            "py": fields.Integer(description="position on the axis Oy"),
        },
    )
