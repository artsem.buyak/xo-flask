from flask_restplus import Namespace, fields


class GameDto:
    api = Namespace("game", description="game related operations")
    game = api.model(
        "game",
        {
            "game_uuid": fields.String(required=True, description="game uuid"),
            "initial_px": fields.String(description="game uuid"),
            "initial_py": fields.String(description="game uuid"),
        },
    )
    game_info = api.model(
        "game",
        {
            "game_uuid": fields.String(required=True, description="game uuid"),
            "status": fields.String(required=True, description="game status"),
            "winner": fields.String(required=False, description="game winner"),
            "created_at": fields.DateTime(
                required=True, description="date of creation"
            ),
            "finished_at": fields.DateTime(
                required=False, description="date of creation"
            ),
        },
    )
