"""auto

Revision ID: 23816d72838d
Revises: 7c86a8c2898a
Create Date: 2021-04-05 01:50:01.553930

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "23816d72838d"
down_revision = "7c86a8c2898a"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "move",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("game_id", sa.Integer(), nullable=True),
        sa.Column("px", sa.Integer(), nullable=False),
        sa.Column("py", sa.Integer(), nullable=False),
        sa.Column("is_user", sa.Boolean(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(
            ["game_id"],
            ["game.id"],
        ),
        sa.PrimaryKeyConstraint("id"),
    )
    op.create_index(op.f("ix_move_game_id"), "move", ["game_id"], unique=False)
    op.add_column("game", sa.Column("created_at", sa.DateTime(), nullable=True))
    op.add_column("game", sa.Column("finished_at", sa.DateTime(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("game", "finished_at")
    op.drop_column("game", "created_at")
    op.drop_index(op.f("ix_move_game_id"), table_name="move")
    op.drop_table("move")
    # ### end Alembic commands ###
