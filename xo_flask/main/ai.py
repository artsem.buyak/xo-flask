class ArtificialIntelligence:
    """
    Realization Minimax with Alpha-Beta Pruning

    Description here https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning
    """

    def __init__(self):
        self.current_state = []
        self.initialize_game()

    def initialize_game(self):
        """
        init start position:
        [".", ".", "."]
        [".", ".", "."]
        [".", ".", "."]
        """
        self.current_state = [["." for _ in range(3)] for _ in range(3)]

    def position_from_moves_list(self, moves):
        """
        From all the moves we build the current position on the board
        """
        for move in moves:
            mark = "X" if move.is_user else "O"

            self.current_state[move.px][move.py] = mark

    def is_valid(self, px, py):
        if px < 0 or px > 2 or py < 0 or py > 2:
            return False
        elif self.current_state[px][py] != ".":
            return False
        else:
            return True

    # Checks if the game has ended and returns the winner in each case
    def is_end(self):

        for i in range(0, 3):
            # Vertical win
            if (
                self.current_state[0][i] != "."
                and self.current_state[0][i] == self.current_state[1][i]
                and self.current_state[1][i] == self.current_state[2][i]
            ):
                return self.current_state[0][i]
            # Horizontal win
            elif self.current_state[i] == ["X" for _ in range(3)]:
                return "X"
            # Vertical win
            elif self.current_state[i] == ["O" for _ in range(3)]:
                return "O"

        # Main diagonal win
        if (
            self.current_state[0][0] != "."
            and self.current_state[0][0] == self.current_state[1][1]
            and self.current_state[0][0] == self.current_state[2][2]
        ):
            return self.current_state[0][0]

        # Second diagonal win
        if (
            self.current_state[0][2] != "."
            and self.current_state[0][2] == self.current_state[1][1]
            and self.current_state[0][2] == self.current_state[2][0]
        ):
            return self.current_state[0][2]

        # Is whole board full?
        for i in range(0, 3):
            for j in range(0, 3):
                # There"s an empty field, we continue the game
                if self.current_state[i][j] == ".":
                    return None

        # It"s a tie!
        return "."

    def min_alpha_beta(self, alpha, beta):
        minv = 2
        qx = None
        qy = None

        result = self.is_end()

        if result == "X":
            return -1, 0, 0
        elif result == "O":
            return 1, 0, 0
        elif result == ".":
            return 0, 0, 0

        for i in range(0, 3):
            for j in range(0, 3):
                if self.current_state[i][j] == ".":
                    self.current_state[i][j] = "X"
                    (m, max_i, max_j) = self.max_alpha_beta(alpha, beta)
                    if m < minv:
                        minv = m
                        qx = i
                        qy = j
                    self.current_state[i][j] = "."

                    if minv <= alpha:
                        return minv, qx, qy

                    if minv < beta:
                        beta = minv

        return minv, qx, qy

    def max_alpha_beta(self, alpha, beta):
        maxv = -2
        px = None
        py = None

        result = self.is_end()

        if result == "X":
            return -1, 0, 0
        elif result == "O":
            return 1, 0, 0
        elif result == ".":
            return 0, 0, 0

        for i in range(0, 3):
            for j in range(0, 3):
                if self.current_state[i][j] == ".":
                    self.current_state[i][j] = "O"
                    (m, min_i, in_j) = self.min_alpha_beta(alpha, beta)
                    if m > maxv:
                        maxv = m
                        px = i
                        py = j
                    self.current_state[i][j] = "."

                    # Next two ifs in Max and Min are the only difference
                    # between regular algorithm and minimax
                    if maxv >= beta:
                        return maxv, px, py

                    if maxv > alpha:
                        alpha = maxv

        return maxv, px, py
